<?php
// variable declaration
$a = 5;

$b = 6;

// output sum
echo $a + $b;

// declaring an array
$numbers = [6, 4, 3, 23, "Arjun", [1, 2, 3]];

// printing array in readable form
print_r($numbers);

// printing array in more readable form: with data types
var_dump($numbers);
//Strings
$firstName = "Ram";
$lastName = "Karki";
// String concatenation
echo "My Name is: " . $firstName . " " . $lastName . ".";
// String interpolation
echo "My Name is: $firstName $lastName.";
// Associative Array
$man = [
    "legs" => 2,
    "hands" => 2,
    "complexion" => "dark",
    "clothes" => [
        "college_uniform" => 2,
        "party_dress" => 38
    ],
];
echo "\n\n";
echo $man["legs"];
echo "\nThis man has " . $man["clothes"]['party_dress'] . " party dresses." ;
var_dump($man) ;
include "functions.php" ;
echo addThese(4, 5) ;