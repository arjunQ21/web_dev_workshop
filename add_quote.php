<?php
$method = $_SERVER['REQUEST_METHOD'] ;
if($method == "POST"){
    $author_name = $_POST["author_name"] ;
    $content =  $_POST["content"] ;
    // insert into quotes values(null, "Albert Einstein", "Time is Relative") ;
    $raw_query = "insert into quotes values(null, '$author_name', '$content') " ;
    include "connection.php" ;

    $result = mysqli_query($conn, $raw_query) ;

    if(mysqli_error_list($conn)){
        echo "Error inserting data" ;
    }else{
        header("Location: quotes.php") ;
        echo "Data inserted" ;
    }

    // echo $raw_query ;
    // $raw_query = "insert into quotes values(null, \"Albert Einstein\", \"Time is Relative\") " ;
    // echo "Received: Author: $author_name, Content: $content" ;
}
?>
<!DOCTYPE html>
<html>
<head>
    <title>Add New Quote</title>
</head>
<body>
    <h1>Add New Quote</h1>
    <form action="" method="POST">
        <p>Author Name:</p>
        <input name="author_name" type="text" placeholder="Enter Author Name Here...">
        <p>Content:</p>
        <input name="content" type="text" placeholder="Enter Quote Content Here...">
        <input type="submit" id='button' value="Add Quote" />
    </form>
</body>
<script>

var authorNameElem = document.querySelector('input[name=author_name]')
var contentElem = document.querySelector('input[name=content]')
var button = document.querySelector('input#button')
console.log({authorNameElem, contentElem, button})

button.addEventListener("click", function(e){
    e.preventDefault() ;
    
console.log("Value in author name: " + authorNameElem.value)

if(!authorNameElem.value || !contentElem.value){
    alert("Please check your inputs.") ;
}else{
    document.querySelector("form").submit() ;
}


})

</script>


</html>