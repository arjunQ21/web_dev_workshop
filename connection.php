<?php

$hostname = "localhost";
$username = "root";
$password = "";
$database_name = "gces_quotes_app";
$port = 3306 ;

$conn = mysqli_connect($hostname, $username, $password, $database_name, $port);

// Check connection
if (mysqli_connect_errno()) {
    echo "Failed to connect to MySQL: " . mysqli_connect_error();
    exit();
}else{
    // echo "Connected to DB";
}
