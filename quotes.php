<?php
include "connection.php";
$raw_query = "select * from quotes order by id desc";
$result = mysqli_query($conn, $raw_query);
if (mysqli_error_list($conn)) {
    echo "Error occurred";
}
$rows = mysqli_fetch_all($result, MYSQLI_ASSOC);
?>
<!DOCTYPE html>
<html>

<head>
    <title>All Quotes</title>
</head>

<body>
    <style>
        div.has-button {
            padding: 20px;
            background-color: #0000ff;
            border: 10px solid rgba(0, 100, 50, 1);
            position: fixed;
            /* bottom: 10px; */
            /* right: 10px; */
            /* transition: 2s; */
        }
    </style>

    <h2>All Quotes</h2>

    <div id='button' class='has-button'>
        <a style="color: green; background: red;" href="add_quote.php">Add Quote</a>
    </div>

    <table>
        <tr>
            <th>
                ID
            </th>
            <th>Author</th>
            <th>Content</th>
        </tr>
        <?php foreach ($rows as $row) : ?>
            <tr>
                <td><?= $row['id'] ?></td>
                <td><?= $row['author'] ?></td>
                <td><?= $row['content'] ?></td>
            </tr>
        <?php endforeach ?>
    </table>
</body>
<script>
    var button = document.getElementById("button");

    // setTimeout(function() {

    //     button.style.bottom = "calc(100vh - 200px)";

    // }, 2000)


    document.addEventListener("DOMContentLoaded", function() {
        console.log("Hello")
    })

    document.addEventListener("mousemove", function(e){
        // console.log(e.clientX, e.clientY)
        button.style.top = e.clientY + "px" ;
        button.style.left = e.clientX + "px" ;
    })
</script>

</html>